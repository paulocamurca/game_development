### Amit’s Game Programming Information

Site with a lot of information about games and algorithms  [link](http://www-cs-students.stanford.edu/~amitp/gameprog.html)


### A Method For 'Growing' Rivers In A Randomly Generated World [link](http://www.pixelenvy.ca/wa/river.html)


### Simple terrain smoothing [link](http://nic-gamedev.blogspot.com/2013/02/simple-terrain-smoothing.html)


### Djikstra's Algorithm: Finding the Shortest Path [link](http://nic-gamedev.blogspot.com/2013/02/simple-terrain-smoothing.html)


### Game Development Math Recipes [link](https://www.gamefromscratch.com/page/Game-Development-Math-Recipes.aspx)